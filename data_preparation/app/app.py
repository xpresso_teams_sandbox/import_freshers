"""
This is the implementation of data preparation for sklearn
"""

import sys
import os
import logging
import warnings

import pandas as pd

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
	AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

warnings.filterwarnings("ignore")

__author__ = "import_freshers"

logger = XprLogger("data_preparation", level=logging.INFO)


class DataPreparation(AbstractPipelineComponent):
	""" Main class for any pipeline job. It is extended from AbstractPipelineComponent
	which allows xpresso platform to track and manage the pipeline.
	User will need to implement following method:
	   -start: This is where the main functionality of the component is initiated.
		  This method has a single parameter - the experiment run ID. This is automatically
		  passed by xpresso.ai as the first argument when the component is run
		-completed: this is called when the main functionality of the component
		  is complete, and results are to be stored if required.


	"""

	def __init__(self, train_file_path):
		super().__init__(name="DataPreparation")
		""" Initialize all the required constants and data here """
		self.features = ['Country', 'Date', 'daily_cases']
		self.Train_Columns = ['Country', 'Date']
		self.Test_Columns = ['daily_cases']
		self.train_data = pd.read_excel(train_file_path)

	@staticmethod
	def preprocessing(each_country, df_uni):
		# get dataframe of one country
		df_each = df_uni[df_uni.Country == each_country]
		# group by done
		df_grouped = df_each.groupby(['Country', 'Date']).Confirmed.sum().reset_index()
		df_confirmed = df_grouped[df_grouped.Country == each_country].Confirmed
		# make confirmed in daily level
		daily_case = list(df_confirmed.diff(periods=1))
		df_grouped['daily_case'] = daily_case
		df_grouped = df_grouped.drop(['Confirmed'], axis=1)
		df_grouped = df_grouped.iloc[1:, :]
		return df_grouped

	def start(self, run_name):
		"""
		This is the start method, which does the actual data preparation.
		As you can see, it does the following:
		  - Calls the superclass start method - this notifies the Controller that
			  the component has started processing (details such as the start
			  time, etc. are appropriately stored by the Controller)
		  - Main data processing or training codebase.
		  - It calls the completed method when it is done

		Args:
			run_name: xpresso run name which is used by base class to identify
			   the current run. It must be passed. While running as pipeline,
			   Xpresso automatically adds it.

		"""
		super().start(xpresso_run_name=run_name)
		# === Your start code base goes here ===
		logging.info("Preparing data...")
		# training_data = self.train_data
		drop_country = ['Diamond Princess', 'MS Zaandam']
		train_data = self.train_data
		train_data = train_data[train_data.Country != drop_country[0]]
		train_data = train_data[train_data.Country != drop_country[1]]
		train_data = train_data.drop(['Province', 'Lat', 'Long'], axis=1)
		country = train_data.Country.unique()
		training_data = pd.DataFrame(columns=['Country', 'Date', 'Confirmed'])
		for each_country in country:
			# print(each_country)
			# get preprocessed dataframe
			df_grouped = DataPreparation.preprocessing(each_country, train_data)
			training_data = training_data.append(df_grouped, ignore_index=True)

		self.send_metrics("Shape of the training data", training_data)

		self.train_data = training_data
		print(self.train_data.tail())
		logging.info("Completed data preparation")

		self.completed()

	def send_metrics(self, status, data):
		""" It is called to report intermediate status. It reports status and
		metrics back to the xpresso.ai controller through the report_status
		method of the superclass. The Controller stores any metrics reported in
		a database, and makes these available for comparison. It needs the
		following format:
		- status:
		   - status - <single word description>
		- metric:
		   - Key-Value - Of the metrics that needs to be tracked and visualized
						 realtime. This could be data size, accuracy, loss etc.
		"""
		report_status = {
			"status": status,
			"metric": {"rows": str(data.shape[0]),
					   "columns": str(data.shape[1])}
		}
		self.report_status(status=report_status)

	def completed(self, push_exp=False):
		"""
		This is the completed method. It stores the output data files on the
		file system, and then calls the superclass completed method, which notes
		the fact that the component has completed processing, along with the end time.

		User must need to call super completed method at the end of the method
		Args:
			push_exp: Whether to push the data present in the output folder
			   to the versioning system. This is required once training is
			   completed and model needs to be versioned

		"""
		# === Your start code base goes here ===
		output_dir = "/data/covid_19_data/"
		if not os.path.exists(output_dir):
			os.makedirs(output_dir)
		self.train_data.to_csv(os.path.join(output_dir, "covid19_train_data.csv"), index=False)
		logging.info("Data saved")
		dat = pd.read_csv(os.path.join(output_dir, "covid19_train_data.csv"))
		print(dat)
		super().completed(push_exp=push_exp)

	def terminate(self):
		"""
		This is used to shutdown the current pipeline execution. All the
		component in the pipeline will be terminated. Once terminated, the
		current pipeline execution cannot be restarted later.

		"""
		# === Your start code base goes here ===
		super().terminate()

	def pause(self, push_exp=True):
		"""
		Pause method is used to pause the execution of the job so that it
		can be restarted at some later point. User should implement this function
		to save the state of the current execution. This state will be used
		on restart.
		Args:
			push_exp: Whether to push the data present in the output folder
			   to the versioning system. This is required once training is
			   completed and model needs to be versioned
		"""
		# === Your start code base goes here ===
		super().pause()

	def restart(self):
		"""
		Restart method is used to start any previously paused experiment. It
		starts the experiment from the same state which was stored when pause
		experiment was called. This should implement the logic to
		reload the state of the previous run.
		"""
		# === Your start code base goes here ===
		super().restart()


if __name__ == "__main__":
	# To run locally. Use following command:
	# XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

	mount_path = "/data"
	train_file_path1 = sys.argv[2]
	path = os.path.join(mount_path, train_file_path1)
	data_prep = DataPreparation(path)
	if len(sys.argv) >= 2:
		data_prep.start(run_name=sys.argv[1])
	else:
		data_prep.start(run_name="")
	# data_prep.start(run_name=sys.argv[1])
