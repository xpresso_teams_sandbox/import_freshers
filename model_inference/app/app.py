"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
import os
import logging
import pickle
import datetime

import pandas as pd
from sklearn.preprocessing import LabelEncoder

from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "import_freshers"

logger = XprLogger("model_inference", level=logging.INFO)


class ModelInference(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """
        self.model = None
        self.output_path = None
        self.Test_Data = None

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        with open(os.path.join(model_path, "covid19_model.pkl"), 'rb') as pf:
            self.model = pickle.load(pf)

    @staticmethod
    def modeling_data_preparation(df):
        df = df.fillna(0)
        df['Country'] = df['Country'].astype('category')
        cat_columns = df.select_dtypes(['category']).columns
        df[cat_columns] = df[cat_columns].apply(lambda x: x.cat.codes)
        df['Date'] = df['Date'].map(datetime.datetime.toordinal)
        return df

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        logging.info("Transforming the testing data")
        mount_path = '/inf_data'
        self.output_path = os.path.join(mount_path, input_request["predictions_location"])
        test_input = pd.read_excel(os.path.join(mount_path, input_request['test_data_file']))
        # print(self.output_path)

        # transform test data file
        no_of_days = 14
        con = test_input['Country'].unique()
        country = []
        date_list = []
        # date = datetime.date.today()
        date = datetime.datetime(2020, 4, 17).date()
        for country_name in con:
            for j in range(no_of_days):
                country.append(country_name)

        for i in range(no_of_days):
            date_list.append(date)
            date = date + datetime.timedelta(days=1)
        date_list = date_list * len(con)

        self.Test_Data = pd.DataFrame(data={'Country': country, 'Date': date_list})

        test_model_data = ModelInference.modeling_data_preparation(self.Test_Data)
        return test_model_data

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        output = self.model.predict(input_request)
        logging.info("Prediction done")
        y_prediction = []
        for item in output:
            if item < 0:
                item = 0
            else:
                item = item
            y_prediction.append(int(round(item)))

        return y_prediction

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        df = self.Test_Data
        df['Confirmed'] = output_response
        output_response = df
        # output_response['Date'] = output_response['Date'].dt.strftime('%Y/%m/%d')
        print(output_response)

        print("prediction_location", os.path.join(self.output_path, "predictions.csv"))
        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)
        output_response.to_csv(os.path.join(self.output_path, "predictions.csv"), index=False)
        test = pd.read_csv(os.path.join(self.output_path, "predictions.csv"))
        print(test)
        return {"prediction_location": self.output_path}


if __name__ == "__main__":
    pred = ModelInference()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="../model_training/model/covid_19_data")
    pred.load()
    pred.run_api(port=5000)
