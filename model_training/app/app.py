"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import os
import pickle

import pandas as pd
import statsmodels.tsa.statespace.structural
import statsmodels as sm
import datetime

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "import_freshers"

logger = XprLogger("model_training", level=logging.INFO)


class ModelTraining(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
	which allows xpresso platform to track and manage the pipeline.
	User will need to implement following method:
	   -start: This is where the main functionality of the component is initiated.
		  This method has a single parameter - the experiment run ID. This is automatically
		  passed by xpresso.ai as the first argument when the component is run
		-completed: this is called when the main functionality of the component
		  is complete, and results are to be stored if required.


	"""

    def __init__(self, train_file_path):
        super().__init__(name="ModelTraining")
        """ Initialize all the required constants and data here"""
        self.Train_Columns = ['Country', 'Date']
        self.Test_Columns = ['daily_cases']
        self.train_data = pd.read_csv(train_file_path)

    @staticmethod
    def model_training(df_grouped, country_name, mod=None):
        regg = ['Afghanistan', 'Algeria', 'Bahamas', 'Bangladesh', 'Belarus', 'Belgium', 'Bolivia',
                'Bosnia and Herzegovina', 'Brunei', 'Burkina Faso', 'Cambodia', 'Cameroon', 'Chad', 'China', 'Chile',
                'India'
                'Colombia', 'Congo (Brazzaville)', 'Cuba', 'Cyprus', 'Djibouti', 'Dominican Republic', 'Ecuador',
                'Egypt', 'El Salvador', 'Eritrea', 'Ethiopia', 'Finland', 'Canada', 'Denmark', 'Georgia', 'Ghana',
                'Sierra Leone',
                'Botswana', 'Kosovo', 'Mali', 'Guinea-Bissau', 'Libya', 'Vietnam', 'Dominica', 'United Kingdom',
                'Uruguay',
                'Uzbekistan', 'Netherlands', 'Nicaragua', 'Niger', 'Nigeria', 'North Macedonia', 'Oman', 'Pakistan',
                'Panama',
                'Paraguay', 'Peru', 'Poland', 'Qatar', 'Rwanda', 'Saint Vincent and the Grenadines', 'Saudi Arabia',
                'Serbia',
                'Singapore', 'Slovakia', 'Sweden', 'Switzerland', 'Togo', 'Turkey', 'Ukraine', 'United Arab Emirates',
                'Lebanon', 'Liberia', 'Madagascar', 'Mauritius', 'Mexico', 'Moldova', 'Monaco', 'Japan',
                'Guinea', 'Honduras', 'Hungary', 'Ireland', 'Jamaica', 'Jordan', 'Kazakhstan',
                'Kenya', 'South Korea', 'Kuwait', 'Kyrgyzstan', 'Brazil', 'India', 'Colombia', 'Guyana'
                ]

        basic = ['Albania', 'Andorra', 'Angola', 'Antigua and Barbuda', 'Argentina', 'Armenia',
                 'Australia', 'Austria', 'Azerbaijan', 'Bahrain', 'Barbados', 'Benin', 'Bhutan',
                 'Bulgaria', 'Cabo Verde', 'Central African Republic', 'Congo (Kinshasa)', 'Costa Rica', 'US'
                                                                                                         'Croatia',
                 'Czechia', 'Equatorial Guinea', 'Estonia', 'Eswatini', 'Fiji', 'France', 'Gabon', 'Gambia',
                 'Germany', 'Greece', 'Russia', 'Burundi', 'Burma', 'Saint Kitts and Nevis', 'Belize', 'Laos',
                 'West Bank and Gaza', 'Mozambique', 'Syria', 'Timor-Leste', 'Zambia', 'Zimbabwe', 'Grenada',
                 'Venezuela',
                 'Montenegro', 'Morocco', 'Namibia', 'Nepal', 'New Zealand', 'Norway', 'Papua New Guinea',
                 'Philippines',
                 'Portugal', 'Romania', 'Saint Lucia', 'San Marino', 'Senegal', 'Seychelles', 'Slovenia', 'Somalia',
                 'South Africa', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Taiwan*', 'Tanzania',
                 'Thailand', 'Trinidad and Tobago', 'Tunisia', 'Uganda', 'US', 'Liechtenstein', 'Lithuania',
                 'Luxembourg',
                 'Malaysia', 'Maldives', 'Malta', 'Mauritania', 'Mongolia', 'Indonesia',
                 'Guatemala', 'Haiti', 'Holy See', 'Iceland', 'Iran', 'Iraq', 'Israel', 'Italy', 'Latvia',
                 "Cote d'Ivoire",
                 'Croatia']

        if country_name in regg:
            mod = sm.tsa.statespace.structural.UnobservedComponents(df_grouped.daily_case,
                                                                    # level='smooth trend',
                                                                    # level='local linear trend',
                                                                    level='random trend',
                                                                    trend=True,
                                                                    seasonal=False, freq_seasonal=False, cycle=True,
                                                                    autoregressive=True, exog=None, irregular=False,
                                                                    stochastic_level=False, stochastic_trend=False,
                                                                    stochastic_seasonal=True,
                                                                    stochastic_freq_seasonal=None,
                                                                    stochastic_cycle=True, damped_cycle=True,
                                                                    cycle_period_bounds=None, mle_regression=True,
                                                                    use_exact_diffuse=False
                                                                    )
        if country_name in basic:
            mod = sm.tsa.statespace.structural.UnobservedComponents(df_grouped.daily_case, level=True, trend=True,
                                                                    seasonal=False, freq_seasonal=False, cycle=False)

        results = mod.fit(disp=False)
        return results

    def start(self, run_name):
        """
		This is the start method, which does the actual data preparation.
		As you can see, it does the following:
		  - Calls the superclass start method - this notifies the Controller that
			  the component has started processing (details such as the start
			  time, etc. are appropriately stored by the Controller)
		  - Main data processing or training codebase.
		  - It calls the completed method when it is done

		Args:
			run_name: xpresso run name which is used by base class to identify
			   the current run. It must be passed. While running as pipeline,
			   Xpresso automatically adds it.

		"""
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        training_data = self.train_data
        logging.info("Model prediction started")
        country = training_data.Country.unique()
        opdir = "model"
        saved_file_name = ""
        for each_country in country:
            df_grouped = training_data[training_data.Country == each_country]
            model = ModelTraining.model_training(df_grouped, each_country)
            if each_country == "Taiwan*":
                saved_file_name = "Taiwan" + 'COVID_ucm.pkl'
            else:
                saved_file_name = each_country + 'COVID_ucm.pkl'
            # for training on full data set
            logging.info("Model stored")
            # if not os.path.exists(opdir):
            #     os.makedirs(opdir)
            # pickle.dump(model, open(os.path.join(opdir, saved_file_name), 'wb'))
            if not os.path.exists(self.OUTPUT_DIR):
                os.makedirs(self.OUTPUT_DIR)
            pickle.dump(model, open(os.path.join(self.OUTPUT_DIR, saved_file_name), 'wb'))

        self.completed(push_exp=True)

    def send_metrics(self, status, value):
        """ It is called to report intermediate status. It reports status and
		metrics back to the xpresso.ai controller through the report_status
		method of the superclass. The Controller stores any metrics reported in
		a database, and makes these available for comparison. It needs the
		following format:
		- status:
		   - status - <single word description>
		- metric:
		   - Key-Value - Of the metrics that needs to be tracked and visualized
						 realtime. This could be data size, accuracy, loss etc.
		"""
        report_status = {
            "status": status,
            "metric": value
        }
        self.report_status(status=report_status)

    def completed(self, push_exp):
        """
		This is the completed method. It stores the output data files on the
		file system, and then calls the superclass completed method, which notes
		the fact that the component has completed processing, along with the end time.

		User must need to call super completed method at the end of the method
		Args:
			push_exp: Whether to push the data present in the output folder
			   to the versioning system. This is required once training is
			   completed and model needs to be versioned

		"""
        # === Your start code base goes here ===

        # print(push_exp)

        super().completed(push_exp=True)

    def terminate(self):
        """
		This is used to shutdown the current pipeline execution. All the
		component in the pipeline will be terminated. Once terminated, the
		current pipeline execution cannot be restarted later.

		"""
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
		Pause method is used to pause the execution of the job so that it
		can be restarted at some later point. User should implement this function
		to save the state of the current execution. This state will be used
		on restart.
		Args:
			push_exp: Whether to push the data present in the output folder
			   to the versioning system. This is required once training is
			   completed and model needs to be versioned
		"""
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
		Restart method is used to start any previously paused experiment. It
		starts the experiment from the same state which was stored when pause
		experiment was called. This should implement the logic to
		reload the state of the previous run.
		"""
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = ModelTraining(train_file_path="/data/covid_19_data/covid19_train_data.csv")
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
