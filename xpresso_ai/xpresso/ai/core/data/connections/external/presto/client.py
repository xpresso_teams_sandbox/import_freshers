"""Class design for Presto Connectivity"""

import json
import prestodb
import pandas as pd
import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger


class PrestoConnector:
    """

    This class is used to interact with the any of the databases as per
    the configurations provided. The supported operation as of now are
    importing & exporting of data.

    """

    def __init__(self, user_input):
        """

        __init__() function initializes the host ip and port for Presto server.
        It also initiatiates the catalog and schema needed for Presto client.

        """

        self.logger = XprLogger()
        self.connector = None
        self.cursor = None
        with open(XprConfigParser().get_default_config_path()) as config_file:
            presto_config = json.load(config_file).get(constants.connectors) \
                .get(constants.presto)
        self.presto_host_ip = presto_config.get(constants.presto_ip)
        self.presto_host_port = presto_config.get(constants.presto_port)
        self.presto_user = presto_config.get(constants.presto_user)
        self.catalog = presto_config.get(constants.DSN).get(user_input).get(constants.catalog)
        self.schema = presto_config.get(constants.DSN).get(user_input).get(constants.schema)

    def get_connector(self):
        """

        This methods creates an connection to database with the provided configurations.

        """

        try:
            connector = prestodb.dbapi.connect(host=self.presto_host_ip, port=self.presto_host_port,
                                               user=self.presto_user, catalog=self.catalog,
                                               schema=self.schema)
        except Exception:
            self.logger.exception("Connection to presto client failed.")
            raise xpr_exp.DBConnectionFailed
        return connector

    def import_data(self, table, columns):
        """

        This method makes an select query.

        Args:
            table (str): table name.
            columns (str/list): names of columns as a comma-separated string.
                Put '*' in case of all columns.

        Returns:
             object: a pandas DataFrame.

        """

        self.connector = self.get_connector()
        query = "select %s from %s"
        self.cursor = self.connector.cursor()
        self.cursor.execute(query % (','.join(columns), table))
        results = self.cursor.fetchall()
        columns = []
        for column in self.cursor.description:
            columns.append(column[0])
        return pd.DataFrame(results, columns=columns)

    def close(self):
        """

        Method used to close all connections to the API.

        """

        self.cursor.close()
        self.connector.close()
